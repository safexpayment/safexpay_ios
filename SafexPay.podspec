Pod::Spec.new do |spec|

  spec.name         = 'SafexPay'
  spec.version      = '1.4.2'
  spec.summary      = 'SafexPay xcframework'
  spec.homepage     = 'https://bitbucket.org/safexpayment/safexpay_ios/src/master/'
  spec.description  = 'SafexPay framework for payments.'
  spec.license      = { :type => 'MIT', :file => 'LICENSE' }
  spec.author             = { 'Nagendra Yadav' => 'nagendra@safexpay.com' }

  spec.platform     = :ios
  spec.swift_version = '5.0'
  spec.ios.deployment_target = '10.0'
  spec.source       = { :git => 'https://Abhijit_safexpay@bitbucket.org/safexpayment/safexpay_ios.git', :tag => "#{spec.version}" }

  spec.requires_arc = true
  spec.vendored_frameworks = 'SafexPayIOSSDK.xcframework'

  spec.exclude_files = "Classes/Exclude"

  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }


end
